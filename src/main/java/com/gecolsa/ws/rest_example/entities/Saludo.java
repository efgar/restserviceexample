package com.gecolsa.ws.rest_example.entities;

import java.util.Date;

public class Saludo {
	public String saludo;
	public String persona;
	public Date dia;

	public Saludo(String saludo, String name) {
		this.saludo = saludo;
		this.persona = name;
		this.dia = new Date();
	}
	public String getSaludo() {
		return saludo;
	}
	public void setSaludo(String saludo) {
		this.saludo = saludo;
	}
	public String getPersona() {
		return persona;
	}
	public void setPersona(String persona) {
		this.persona = persona;
	}
	public Date getDia() {
		return dia;
	}
	public void setDia(Date dia) {
		this.dia = dia;
	}
	
}
