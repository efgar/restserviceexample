package com.gecolsa.ws.rest_example.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.gecolsa.ws.rest_example.entities.Saludo;

@RestController()
@RequestMapping("/hello")
public class MyAwesomeController {
	
	@RequestMapping(method=RequestMethod.GET)
	public String sayHelloExpanded(@RequestParam(name="name") String name){
		if (name == null){
			name = "";
		}
		return "Hello! " + name;
	}

	@RequestMapping(path = "/kitty", method=RequestMethod.GET)
	public String sayHelloKitty(){
		return "Hello Kitty!!!";
	}
	
	@RequestMapping("/greeting")
	public @ResponseBody Saludo greeting(@RequestParam(name="name") String name){
		Saludo hola = new Saludo ("Buenas ", name);
		return hola;
	}
}
