package com.gecolsa.ws.rest_example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
    	System.getProperties().put( "server.port", 9090);
        SpringApplication.run(Application.class, args);
    }

}