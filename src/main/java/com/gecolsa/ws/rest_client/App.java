package com.gecolsa.ws.rest_client;

import org.springframework.web.client.RestTemplate;

import com.gecolsa.ws.rest_client.entities.Quote;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	RestTemplate restTemplate = new RestTemplate();
    	Quote result = restTemplate.getForObject("http://gturnquist-quoters.cfapps.io/api/random", Quote.class);
        System.out.println(result);
        System.out.println(result.getType());
    }
}
